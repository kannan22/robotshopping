import {
  observable,
  action,
  computed,
  observe,
  makeAutoObservable,
} from "mobx";
import ProductModel from "./models/productmodel";
import ShoppingContext from "./models/shoppingContext";

export default class ShoppingStore {
  constructor(shoppingService) {
    this.service = shoppingService;
    makeAutoObservable(this);
  }

  @observable products;
  @observable productsList = [];
  @observable productCards = {};
  @observable materialFilter = [];
  @observable shoppingContext = new ShoppingContext();
  @observable itemCountinCart = "";
  @observable CartproductsList = [];
  @observable isCartOpen = false;
  @action GetProducts() {
    let self = this;
    let promise = this.service.GetProducts();
    
    promise.then(
      action("fetchSuccess", (res) => {
        
        self.productsList = [];

        Object.keys(res.data.data).map((key, index) => {
          let row = res.data.data[index];
          self.productsList.push(ProductModel.fromJson(row));
        });
        self.products = self.productsList;
      }),
      action("fetchError", (error) => {
        console.log(error);
      })
    );
    return promise;
  }

  @action getmaterialList() {
    var filterMaterials = [
      ...new Set(
        this.productsList.map((m) => {
          var fm = [];
          fm["material"] = m.material;
          return fm;
        })
      ),
    ];
    this.materialFilter = filterMaterials;
  }
  @action CheckproductinCart(product) {
    
    var result = true;
    let self = this;
  }

  @action CheckMaxLimit(product) {
    
    var result = true;
    let self = this;
    let pc = self.productCards;
    var value = pc[product.name];
    var pccount = Object.keys(pc).length;
    if (pccount < 5 || value >= 0) {
      result = false;
    } else {
      result = true;
    }
    return result;
  }

  @action CheckandAddQnty(product) {
    
    var result = true;
    let self = this;
    let pc = self.productCards;
    var value = pc[product.name];
    if (value === undefined) {
      value = 0;
    }
    if (value > 0) {
      if (value >= product.stock) {
        result = false;
      } else {
        pc[product.name] = value + 1;
      }
    } else {
      pc[product.name] = value + 1;
    }
    self.productCards = pc;
    return result;
  }
  @action shouldDisableAddtoCart(product) {
    var result = false;
    let self = this;
    let pc = self.productCards;
    var value = pc[product.name];
    if (value === undefined) {
      value = 0;
    }
    if (value == product.stock) {
      result = true;
    } else {
      result = false;
    }
    return result;
  }
  @action addProducttoCheckOut(product) {
    
    let self = this;
    let pc = self.productCards;
    var value = pc[product.name];
    var len = self.CartproductsList.length;
    if (len > 0) {
      let checkProduct = self.CartproductsList.filter(
        (item) => item.Product.name == product.name
      );
      if (
        checkProduct == null ||
        checkProduct === undefined ||
        checkProduct.length == 0
      ) {
        var pro = { Product: product, Qty: value };

        self.CartproductsList.push(pro);
      } else {
        
        var filtered = self.CartproductsList.filter(function (
          value,
          index,
          arr
        ) {
          return value.Product.name != product.name;
        });
        self.CartproductsList = filtered;
        var pro = { Product: product, Qty: value };
        self.CartproductsList.push(pro);
        self.CartproductsList.sort();
      }
    } else {
      var pro = { Product: product, Qty: value };
      self.CartproductsList.push(pro);
    }
  }

  @action filterProductsbymetarial(value) {
    
    let self = this;
    if (value.length > 3) {
      var filterd = self.productsList.filter((vl, index) => {
        return vl.material.toLowerCase().indexOf(value) !== -1;
      });
      self.productsList = filterd;
    } else {
      self.productsList = self.products;
    }
  }
}
