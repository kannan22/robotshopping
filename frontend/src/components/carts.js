import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { observable, action, computed } from "mobx";
import CartItem from "./cartitem";


@inject("shoppingStore")
@observer
class Cart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      totprice: 0,
    };
  }
  CalcTotal() {
    let total = 0.0;
    this.props.shoppingStore.CartproductsList.map((product, id) => {
      let p = parseFloat(product.Product.price);
      let q = product.Qty;
      let tp = parseFloat((q * p).toFixed(2));
      total = total + tp;
    });
    // this.setState({
    //   totprice: total,
    // });
    return total.toFixed(2);
  }

  render() {
    return (
      <div>
        <h3>Checkout Robots</h3>
        <div className="modal-body">
          <div>
            <ul>
              {this.props.shoppingStore.CartproductsList.map((product, id) => {
                return (
                  <li key={id + 1}>
                    <CartItem product={product.Product} Qty={product.Qty} />
                  </li>
                );
              })}
            </ul>
            <hr />
            {this.props.shoppingStore.CartproductsList.length === 0 && (
              <div className="alert alert-info">Cart is empty</div>
            )}
            {this.props.shoppingStore.CartproductsList.length > 0 && (
              <div>
                <div className="cart__total">Total: ฿{this.CalcTotal()}</div>
                <hr />
                <div>
                  <button
                    className="btn btn-primary moveright"
                    onClick={() => {
                      alert("Thank you for shopping robots.");
                    }}
                  >
                    Check out
                  </button>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}
export default Cart;
