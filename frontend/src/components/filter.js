import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { observable, action, computed } from "mobx";
import Cart from "./carts";


@inject("shoppingStore")
@observer
class Filter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: "",
    };
  }

  FilterProducts = (value) => {
    this.setState({
      filter: value,
    });
    this.props.shoppingStore.filterProductsbymetarial(value);
  };

  render() {
    return (
      <div className="row ">
        <div className="col-md-4">{`${this.props.shoppingStore.productsList.length} products found.`}</div>
        <div className="form-inline ">
          <div className="form-group pd-left ">
            Filter by :
            <input
              className="pd-left mar-left"
              type="text"
              value={this.state.filter}
              onChange={(e) => this.FilterProducts(e.target.value)}
              placeholder="Material"
            ></input>
          </div>
          * Type first 3 letters of material to filter the robots.
        </div>
      </div>
    );
  }
}
export default Filter;
