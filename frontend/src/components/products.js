import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { observable, action, computed } from "mobx";


@inject("shoppingStore")
@observer
class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      productsinCart: "",
      isOutofStock: false,
    };
  }
  componentWillMount() {
    
    this.props.shoppingStore.GetProducts();
  }
  componentDidMount() {
    
    var products = this.props.shoppingStore.productsList;
    this.setState({ productsls: products });
  }
  addtocart = (product) => {
    
    var limitExceed = this.props.shoppingStore.CheckMaxLimit(product);
    if (limitExceed) {
      alert("Oops! , you cannot add more than 5 different robots.");
      return;
    } else {
      var isAdded = this.props.shoppingStore.CheckandAddQnty(product);
      if (!isAdded) {
        //alert("Out of Stock");
        this.setState({
          isOutofStock: true,
        });
        this.button.disabled = true;
      }
    }
    var pccount = Object.keys(this.props.shoppingStore.productCards).length;
    // this.setState({
    //   productsinCart: pccount,
    // });
    this.props.shoppingStore.itemCountinCart = pccount;
    this.props.shoppingStore.addProducttoCheckOut(product);
  };

  removefromCart=(product)=>{
      
  };
  isdisabled = (product) => {
    
    let isdisabled = this.props.shoppingStore.shouldDisableAddtoCart(product);
    return isdisabled;
  };
  render() {

    return (
      <div>
        {this.props.shoppingStore.productsList.map((product, id) => {
          return (
            <div className="col-md-3">
              <div className="thumbnail text-center" key={id}>
                <img src={product.image} alt={product.name} />
                <h5>{product.name}</h5>
                <h6>Price: ฿{product.price}</h6>
                <h6>Stock: {product.stock}</h6>
                <h6>Created on: {product.createdAt}</h6>
                <h6>Material: {product.material}</h6>
                <button
                  className="btn btn-primary"
                  onClick={() => this.addtocart(product)}
                  disabled={this.isdisabled(product)}
                >
                  Add to Cart
                </button>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}
export default Products;
