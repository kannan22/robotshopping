import React, { Component } from "react";
import { inject, observer } from "mobx-react";
import { observable, action, computed } from "mobx";

@inject("shoppingStore")
@observer
class CartItem extends Component {
  constructor(props) {
    super(props);

    this.state = {
      quantity: this.props.Qty,
      totprice: parseFloat(this.props.product.price),
    };
  }
  UNSAFE_componentWillReceiveProps(nextProps) {
    
    var price = (nextProps.Qty * parseFloat(nextProps.product.price)).toFixed(
      2
    );
    this.setState({
      quantity: nextProps.Qty,
      totprice: price,
    });
  }
  IncrementItem = (stock, price) => {
    
    let st = this.state.quantity;
    if (st < stock) {
      st = st + 1;
      this.setState({ quantity: st });
    } else {
      alert("Out of stock");
    }
    setTimeout(() => {
      this.handleChange(price);
    }, 500);
  };
  DecreaseItem = (stock, price) => {
    
    let st = this.state.quantity;
    if (st > 0) {
      st = st - 1;
      this.setState({ quantity: st });
    } else {
      this.setState({ quantity: 0 });
    }
    setTimeout(() => {
      this.handleChange(price);
    }, 500);
  };
  handleChange = (price) => {
    
    let qty = parseFloat(this.state.quantity);
    let tp = (qty * price).toFixed(2);
    this.setState({
      totprice: tp,
    });
    return tp;
  };
  render() {
    
    const { product, Qty } = this.props;
    
    return (
      <div className="cart-item">
        <div>
          <img src={product.image} alt={product.name} />
          <br></br>
          <span className="cart-item__name">{product.name}</span>
        </div>

        <div className="cart-item__price">
          <b> ฿{product.price}</b>
        </div>

        <div>
          <button
            className="btn-xs btn btn-primary"
            onClick={() => this.IncrementItem(product.stock, product.price)}
          >
            +
          </button>

          <input
            style={{ width: 25 + "px", height: 20 + "px" }}
            className="inputne"
            value={this.state.quantity}
            onChange={(e) => this.handleChange(product.price)}
          />
          <button
            className="btn-xs btn btn-primary"
            onClick={() => this.DecreaseItem(product.stock, product.price)}
          >
            -
          </button>
        </div>
        <div className="cart-item__price">฿{this.state.totprice}</div>
      </div>
    );
  }
}
export default CartItem;
