import React, { Component } from "react";
import Products from "./components/products";
import Filter from "./components/filter";
import { inject, observer } from "mobx-react";
import Cart from "./components/carts";

@inject("shoppingStore")
@observer
class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>Robots Shopping</h1>
            <hr />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <Filter />
            <hr />
          </div>
        </div>
        <div className="row">
          {/* <div className="col-md-12">
            <Filter />
            <hr />
          </div> */}
          <div>
            <div className="col-md-8">
              <Products />
            </div>
            <div className="col-md-4">
              <Cart />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
