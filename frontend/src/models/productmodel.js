import { observable } from "mobx";
import moment from "moment";

export default class ProductModel {
  @observable name;
  @observable image;
  @observable price;
  @observable stock;
  @observable createdAt;
  @observable material;

  static fromJson(jsondata) {
    let model = new ProductModel();
    model.name = jsondata.name;
    model.image = jsondata.image;
    model.price = jsondata.price;
    model.stock = jsondata.stock;
    model.createdAt = moment(jsondata.createdAt).format("DD-MM-YYYY");
    model.material = jsondata.material;
    return model;
  }
}
