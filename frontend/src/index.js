import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { Provider } from "mobx-react";
import ShoppingStore from "./ShoppingStore";
import { ShoppingService } from "./ShoppingService";

let shoppingStore = new ShoppingStore(new ShoppingService());

ReactDOM.render(
  <Provider shoppingStore={shoppingStore}>
    <App />
  </Provider>,
  document.getElementById("root")
);
